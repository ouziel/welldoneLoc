import React, {PropTypes} from 'react';
import {validate, clearField} from 'util';
import LocationMap from 'components/Map';

export default class Form extends React.Component {
    constructor(props) {
        super(props);
        // new location state
        // keys === refs
        const blank = {
            name: '',
            address: '',
            longitude: '',
            latitude: '',
            category: '',
            categories: []
        }
        // edit location  state
        const {query} = this.props;
        /// assign state
        const state = (query)
            ? {
                ...query
            }
            : blank
        this.state = state;
    }

    submitForm(e) {
        e.preventDefault();
        const {category, name, address, latitude, longitude} = this.refs;
        let fieldsArray = Object.keys(this.refs).map(ref => {
            const field = this.refs[ref];
            if (!validate(field)) {
                field.focus();
                clearField(field)
                return false;
            } else {
                return true;
            }
        });
        // if valid..
        const categories = this.state.categories;
        if (!fieldsArray.includes(false)) {
            this.props.add({
                categories,
                id: this.state.id,
                category: category.value,
                name: name.value,
                address: address.value,
                latitude: latitude.value,
                longitude: longitude.value
            })
        };
    }

    onChange(ref) {
        this.setState({[ref]: this.refs[ref].value})
    }

    changeLatLong({latitude, longitude}) {
        this.setState({latitude, longitude});
    }
    setOptions() {
        const {category} = this.refs,
            options = category.options,
            categories = Array.from(options).filter(x => x.selected === true).map(x => x.value);
        this.setState({categories});

    }
    componentDidMount() {

        this.setInitialValues();

    }
    setInitialValues() {

        const {query} = this.props;
        const {category} = this.refs;
        // if in eidt mode, set selected categories
        if (query && query.edit) {
            const {categories} = query;
            Array.from(category.options).forEach(opt => {
                if (categories.findIndex(cat => cat === opt.value) > -1) {
                    opt.selected = true;
                }
            })
            if (categories.length && !categories.includes(null)) {
                this.setState({categories, id: query.id})
            }
        }
    }
    render() {

        const {categories, query} = this.props;
        const {longitude, latitude} = this.state;
        return (
            <div className="edit-panel">
                <LocationMap longitude={longitude} latitude={latitude} change={this.changeLatLong.bind(this)}/>
                <form className="new-location" onSubmit={this.submitForm.bind(this)}>
                    {categories && <div className="categories-row">
                        <select ref="category" onChange={this.setOptions.bind(this)} multiple>
                            {[...categories].map((category, i) => {
                                return (<option value={category} key={i} label={category}/>)
                            })}
                        </select>
                        <span className="hint">press Ctrl to add multiple categories</span>
                    </div>}

                    <div className="row">
                        <label>
                            Name of Location
                        </label>
                        <input type="text" ref="name" value={this.state.name} onChange={this.onChange.bind(this, 'name')}/>
                    </div>
                    <div className="row">
                        <label>
                            Address
                        </label>
                        <input type="text" ref="address" ref="address" value={this.state.address} onChange={this.onChange.bind(this, 'address')}/>
                    </div>
                    <div className={'row ' + 'lat-long'}>
                        <div className="row">
                            <label>
                                Longitude
                            </label>
                            <input disabled className="longitude" type="number" value={this.state.longitude} ref="longitude" onChange={this.onChange.bind(this, 'longitude')}/>

                        </div>
                        <div className="row">
                            <label>
                                Latitude
                            </label>
                            <input disabled type="number" ref="latitude" value={this.state.latitude} onChange={this.onChange.bind(this, 'latitude')}/>
                        </div>

                    </div>
                    <div className="row">
                        <input type="submit" value="submit" className="submit-btn" required/>
                    </div>
                </form>
            </div>
        );
    }
}

Form.propTypes = {};
