import React, {PropTypes} from 'react';
import ReactCSSTransitionGroup from 'react-addons-css-transition-group';
import Nav from 'components/Nav';
import Symbols from 'components/Symbols';
import {pathname} from 'util';
import Notifications, {notify} from 'react-notify-toast';
const App = ({children, location}) => {
    const pagename = pathname(location.pathname);
    return (
        <div className={'app ' + `page-${ (pagename)
            ? pagename
            : 'index'}`}>
            <Symbols/>
            <ReactCSSTransitionGroup component="main" transitionName="fade" transitionEnterTimeout={500} transitionLeaveTimeout={500}>
                {React.cloneElement(children, {key: location.pathname})}
            </ReactCSSTransitionGroup>
            <Nav/>
            <Notifications/>
        </div>
    )
}

export default App;
