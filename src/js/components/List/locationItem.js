import React, {PropTypes} from 'react';
import Icon from 'shared/Icon';
import {validate, clearField, pushToQueue} from 'util';
import ToggleBox from 'components/ToggleBox';
import isWindowWidthMobile from 'util/isMobile';

export default class LocationItem extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isEditing: false,
            checked: false,
            isOpen: false,
            isMobile: isWindowWidthMobile()
        }
    }
    componentDidMount() {
        window.addEventListener('resize', this.checkIfMobile.bind(this));
        //  remove event listener
    }

    checkIfMobile() {
        const isMobile = isWindowWidthMobile();
        this.setState({isMobile, isOpen: false})
    }
    toggleCheckbox(id) {
        const checked = !this.state.checked;
        this.setState({checked});
        this.props.toggle(id);
        if (!checked && this.state.isOpen && isWindowWidthMobile()) {
            this.toggleItem();
        }
    }
    toggleItem() {
        this.setState({
            isOpen: !this.state.isOpen
        })
    }
    renderButtons() {
        const {item, remove, edit, show} = this.props;

        let styles = {
            display: (this.state.isMobile)
                ? (this.state.checked)
                    ? 'block'
                    : 'none' : 'block'
        }
        const stylesInfo = {
            display: (this.state.isMobile)
                ? 'none'
                : 'inline-block'
        }
        return (
            <div className="actions" style={styles}>
                <Icon name="map" onClick={() => show(item)}/>
                <Icon name="more" className="read-more" style={stylesInfo} onClick={this.toggleItem.bind(this)}/>
                <Icon name="edit" onClick={() => edit(item)}/>
                <Icon name="delete" onClick={() => remove(item)}/>
            </div>

        )
    }
    render() {
        const {item} = this.props;
        return (
            <li className={this.state.isOpen
                ? 'location-item toggled'
                : 'location-item'}>
                <span className="checkbox">
                    <input checked={this.state.checked} ref={item.id} type="checkbox" onClick={this.toggleCheckbox.bind(this, item.id)}/>
                </span>

                <span className="text" ref="text">
                    <span>{item.name}</span>

                    {(!this.state.isMobile || this.state.isOpen) && <ToggleBox open={this.state.isOpen}>
                        <div className="item-info">
                            <div className="row">
                                <span>{item.categories.length > 1
                                        ? 'Categories'
                                        : 'Category'}:</span>
                                <span>{item.categories.join(',')}</span>
                            </div>
                            <div className="row">
                                <span>Adddress:</span>
                                <span>{item.address}</span>

                            </div>
                        </div>
                    </ToggleBox>}
                </span>

                {this.renderButtons()}
            </li>
        );
    }
}
