import React, {PropTypes} from 'react';
import {Link} from 'react-router';

import Icon from 'shared/Icon';
const PageTitle = (props) => {
    return (
        <h1 className="page-title" style={props.style}>
            {props.text}
        </h1>
    )
}
export default PageTitle;
