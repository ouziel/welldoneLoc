import React, {PropTypes} from 'react';
import Btn from 'components/Btn';
import {validate, clearField} from 'util';
import Icon from 'shared/icon';
import Dropdown from 'muicss/lib/react/dropdown';
import DropdownItem from 'muicss/lib/react/dropdown-item';
import PageTitle from 'components/PageTitle';
import {isMobile} from 'util';

export default class LocationsToolbar extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            category: ''
        }
    }
    changeCategory(category) {
        this.setState({category})
        this.props.filter(category);
    }
    componentDidMount() {
        this.changeCategory('Categories');
    }

    renderDropdown({currentCategory, categories}) {
        return (
            <span className="drop-down">
                <Dropdown label={(currentCategory)
                    ? currentCategory
                    : 'All'} ref="category">
                    <DropdownItem link="#" key='All' ref='all' onClick={this.changeCategory.bind(this, 'all')}>All</DropdownItem>
                    {[...categories.data].map(category => {
                        return (
                            <DropdownItem link="#" key={category} ref={category} onClick={this.changeCategory.bind(this, category)}>{category}</DropdownItem>
                        )
                    })}
                </Dropdown>
            </span>
        )
    }
    renderButtons() {

        const {
            remove,
            sort,
            openNew,
            categories,
            title,
            locations
        } = this.props;

        const {
            locations: {
                indexes,
                data,
                visibles,
                currentCategory
            }
        } = this.props;

        const stylesDelete = {
            display: (!indexes.size)
                ? 'none'
                : 'block'
        }
        const stylesSort = {
            display: (visibles.length > 1)
                ? 'block'
                : 'none'
        }

        return (
            <div className="toolbar-buttons">
                {data.length
                    ? this.renderDropdown({currentCategory, categories})
                    : null}
                <div className="buttons">
                    <Btn style={stylesDelete} text="Delete" color='danger' icon="delete" onClick={remove}/>
                    <Btn style={stylesSort} text="Sort" color='accent' icon="sort" onClick={sort}/>
                    <Btn text="New" icon="add" onClick={openNew}/>
                </div>
            </div>

        )
    }
    render() {

        const {title, blank, goBack} = this.props;
        const styles = {
            display: (isMobile() && !blank)
                ? 'none'
                : 'block'
        }
        return (

            <nav className="toolbar">
                <div className="inner">
                    <PageTitle style={styles} text={title}/> {!blank && this.renderButtons()}
                    {blank && <div className="go-back">
                        <div className="svg-icon" onClick={goBack}>
                            <svg viewBox="0 0 24 24">
                                <path d="M12 4l-1.41 1.41L16.17 11H4v2h12.17l-5.58 5.59L12 20l8-8z"/>
                            </svg>
                        </div>
                    </div>}
                </div>
            </nav>

        );
    }
}
