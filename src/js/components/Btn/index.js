import React, {PropTypes} from 'react';
import Button from 'muicss/lib/react/button';
import Icon from 'shared/Icon';
const Btn = (props) => {
    const {
        text,
        icon,
        onClick,
        color = 'primary',
        children
    } = props;
    return (
        <Button style={props.style} color={color} onClick={onClick
            ? onClick.bind(this)
            : null}>
            <span className="desktop">
                {text}
            </span>
            <span className="mobile">
                {icon
                    ? <Icon name={icon}/>
                    : children}

            </span>
        </Button>
    )
}
export default Btn;
