import {
    ADD_LOCATION,
    SAVE_EDITED,
    DELETE_LOCATION,
    EDIT_LOCATION,
    DELETE_SELECTED_LOCATIONS,
    TOGGLE_LOCATION_INDEX,
    FILTER_VISIBLES,
    SHOW_ALL,
    CHANGE_LOCATION_CATEGORY,
    REMOVE_CATEGORY_LOCATIONS,
    CHANGE_CURRENT_CATEGORY,
    SORT_LOCATIONS,
    REMOVE_CATEGORY_FROM_LOCATION
} from 'constants/locations';
const initialState = {
    data: [],
    visibles: [],
    indexes: new Set(),
    lastIndexEdited: '',
    currentCategory: ''
};
import {log, sort} from 'util';

function replaceCategories(item) {}
const LocationsReducer = (state = initialState, action) => {
    switch (action.type) {
        case CHANGE_CURRENT_CATEGORY:
            {
                const currentCategory = action.payload.category;
                const {indexes} = state;
                indexes.clear();
                return {
                    ...state,
                    currentCategory
                }
            }
        case REMOVE_CATEGORY_LOCATIONS:
            {
                const data = state.data.map(location => {
                    if (location.categories.includes(action.payload.name)) {
                        const newCategories = location.categories.filter(cat => cat !== action.payload.name)
                        location.categories = newCategories;
                    }
                    return location;
                })
                return {
                    ...state,
                    data
                }
            }
        case CHANGE_LOCATION_CATEGORY:
            {

                const {name, newName} = action.payload;
                const data = state.data.map(location => {
                    if (location.categories.includes(name)) {
                        const index = location.categories.findIndex(x => x === name);
                        location.categories.splice(index, 1, newName);
                    }
                    return location;
                })
                return {
                    ...state,
                    data
                }

            }
        case SORT_LOCATIONS:
            {
                const visibles = state.visibles.sort((a, b) => sort(a.name, b.name));
                return {
                    ...state,
                    visibles
                }

            }
        case SHOW_ALL:
            {
                const data = state.data.filter(x => (x && x.categories.length && !x.categories.includes(null))),
                    visibles = data;
                return {
                    ...state,
                    data,
                    visibles
                }

            }
        case FILTER_VISIBLES:
            {
                const category = state.currentCategory,
                    data = state.data,
                    visibles = data.filter(item => (item.categories.findIndex(cat => cat === category) > -1));

                return {
                    ...state,
                    visibles
                }
            }
        case SAVE_EDITED:
            {
                const data = [...state.data].map(location => {
                    if (location.id === state.lastIndexEdited) {
                        return action.payload.item;
                    }
                    return location;
                })
                return {
                    ...state,
                    data,
                    lastIndexEdited: ''
                }
            }
        case EDIT_LOCATION:
            {
                return {
                    ...state,
                    lastIndexEdited: action.payload.item.id
                }
            }
        case TOGGLE_LOCATION_INDEX:
            {
                let indexes = state.indexes;
                const index = action.payload.index;
                if (indexes.has(index)) {
                    indexes.delete(index)
                } else {
                    indexes = indexes.add(index);
                }
                return {
                    ...state,
                    indexes
                }
            }

        case REMOVE_CATEGORY_FROM_LOCATION:
            {

                const {categories, name, id} = action.payload.item;
                const {currentCategory, indexes} = state;
                const newCategories = categories.filter(x => x !== currentCategory);
                const data = state.data.map(item => {
                    if (item.id === id) {
                        item.categories = newCategories;
                        indexes.delete(item.id); // mutates state..
                    }
                    if (!newCategories.includes(null)) {
                        return item;
                    }
                })
                return {
                    ...state,
                    data
                }
            }
        case DELETE_SELECTED_LOCATIONS:
            {
                const data = [...state.data].filter(item => !state.indexes.has(item.id));
                state.indexes.clear(); //  clears the state indexes...
                return {
                    ...state,
                    data
                }
            }

        case DELETE_LOCATION:
            {

                const data = [...state.data].filter(item => item.id !== action.payload.item.id);
                state.indexes.delete(action.payload.item.id)
                return {
                    ...state,
                    data
                }
            }
        case ADD_LOCATION:
            {
                return {
                    ...state,
                    data: [
                        ...state.data,
                        action.payload
                    ]
                }
            }
        default:
            return state;
    }
}
export default LocationsReducer
