export default function clearField(elm, focus = false) {
    if (elm) {
        elm.value = '';
        if (focus) {
            elm.focus();
        }
    }
}
