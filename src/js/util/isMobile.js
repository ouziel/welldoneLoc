import config from 'config';
const isMobile = () => window.outerWidth <= config.mobileWidth
export default isMobile
