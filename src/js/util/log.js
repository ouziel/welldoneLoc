import config from 'config';
const log = (x) => (!config.production)
    ? console.log(x)
    : null;

export default log;
