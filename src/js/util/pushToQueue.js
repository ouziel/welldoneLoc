export default function pushToQueue(cb) {
    return setTimeout(() => cb(), 0);
}
