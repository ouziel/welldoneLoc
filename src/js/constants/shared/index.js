const SORT = 'SORT';
const REFRESH_VISIBLES = 'REFRESH_VISIBLES';
const SAVED_TO_DB = 'SAVED_TO_DB';
export {SORT, REFRESH_VISIBLES, SAVED_TO_DB};
