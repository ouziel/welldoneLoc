import {SAVE_EDITED, LOCATION_EXSITS} from 'constants/locations';
import {browserHistory} from 'react-router';
import setDB from '../setDB';
import {filterVisibles} from './filterVisibles';
import {notify} from 'react-notify-toast';
import {isMobile} from 'util';
function saveEdited(item) {
    return (dispatch, getState) => {

        const state = getState();
        const {
            locations: {
                data,
                lastIndexEdited
            }
        } = state;
        const {name, categories, id} = item;
        // check if location name already exsits in one of the new categories
        const shouldSave = !data.some(location => location.categories.some(cat => categories.includes(cat) && name === location.name && location.id !== id))
        if (!shouldSave) {
            if (isMobile()) {
                notify.show('Location exsits', 'error', 4200); // cat ^^
            } else {
                notify.show('Location name already exsits in selected category', 'error', 4200); // cat ^^
            }
            dispatch({type: LOCATION_EXSITS})
        } else if (shouldSave) {
            notify.show('Location saved', 'success', 2500);
            dispatch({type: SAVE_EDITED, payload: {
                    item
                }})
            dispatch(setDB('locations'))
            browserHistory.push({pathname: '/locations'});
            dispatch(filterVisibles(item.category))
        }

    }
}
export default saveEdited;
