import {browserHistory} from 'react-router';
import {FILTER_VISIBLES, SHOW_ALL, REFRESH_VISIBLES, CHANGE_CURRENT_CATEGORY} from 'constants/locations';
import {SORT} from 'constants/shared';
function showAll() {
    return (dispatch) => {

        dispatch({
            type: CHANGE_CURRENT_CATEGORY,
            payload: {
                category: 'All'
            }
        })
        dispatch({type: SHOW_ALL})
    }

}
function filterVisibles(category) {

    return (dispatch, getState) => {
        const state = getState();
        const {categories: {
                data
            }, locations: {
                currentCategory
            }} = state;
        if (category && data.has(category)) {
            dispatch({type: CHANGE_CURRENT_CATEGORY, payload: {
                    category
                }})
            dispatch({type: FILTER_VISIBLES})
        } else if (category && !data.has(category)) {

            dispatch(showAll())
        } else if (!category && data.has(currentCategory)) {

            dispatch({
                type: FILTER_VISIBLES,
                payload: {
                    category: currentCategory
                }
            })

        } else {

            dispatch(showAll())
        }
    }

}
export {showAll, filterVisibles}
