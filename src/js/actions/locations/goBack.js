import {browserHistory} from 'react-router';
import {GO_BACK} from 'constants/locations';
function goBack() {
    return (dispatch) => {
        dispatch({type: GO_BACK})
        browserHistory.goBack();
    }
}
export default goBack
