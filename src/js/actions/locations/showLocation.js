import {SHOW_LOCATION_ON_MAP} from 'constants/locations';
import {browserHistory} from 'react-router';
function showLocation(item) {
    return (dispatch) => {

        const {longitude, latitude} = item;
        dispatch({
            type: SHOW_LOCATION_ON_MAP,
            payload: {
                latitude,
                longitude
            }
        })
        browserHistory.push({
            pathname: '/locations',
            query: {
                view: true,
                latitude,
                longitude

            }
        });

    }
}
export default showLocation;
