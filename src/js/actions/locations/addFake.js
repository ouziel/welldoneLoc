import addLocation from './addLocation';
import {ADD_FAKE_LOCATIONS} from 'constants/locations';
import addFakeCategories from '../categories/addFake';
import Guid from 'guid';
import {rand} from 'util';

function addFakeLocations() {
    const max = 20;
    const IDS = [];
    for (let i = 0; i < max; i++) {
        let guid = Guid.raw();
        let name = guid.substr(0, rand(1, 10));
        IDS.push(name);
    }
    return (dispatch, getState) => {

        const state = getState();
        const {categories, locations} = state;
        const categoriesData = categories.data;
        if (!categoriesData.size) {
            dispatch(addFakeCategories())

            dispatch(addFakeLocations());

        } else {
            dispatch({type: ADD_FAKE_LOCATIONS})
            const cd = categoriesData;
            const locationsData = locations.data;
            const cl = [...categoriesData];
            let categories;
            for (let i = 0; i < max; i++) {
                let item;
                let name = IDS[i];
                const arr = Array.from(Array(rand(1, cl.length)));
                categories = arr.map(x => x = cl[rand(0, cl.length)]);
                item = {
                    name,
                    category: categoriesData[0],
                    categories,
                    address: `some street num`,
                    latitude: '32.30618368655321',
                    longitude: '34.97144554942315'
                }
                dispatch(addLocation(item));
            }

        }
    }

}
export default addFakeLocations;
