import {TOGGLE_LOCATION_INDEX} from 'constants/locations';

function toggleLocationIndex(index) {
    return {type: TOGGLE_LOCATION_INDEX, payload: {
            index
        }}
}
export default toggleLocationIndex
