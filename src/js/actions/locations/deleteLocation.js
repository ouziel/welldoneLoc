import {DELETE_LOCATION, REMOVE_CATEGORY_FROM_LOCATION} from 'constants/locations';
import {filterVisibles} from './filterVisibles';
import setDB from '../setDB';
function deleteLocation(item) {

    return (dispatch, getState) => {

        const state = getState();

        const {categories: {
                data
            }, locations: {
                currentCategory
            }} = state;

        if (data.has(currentCategory)) {

            // delete from single location
            if (item.categories.length < 2) {
                // there is only one category
                dispatch({type: DELETE_LOCATION, payload: {
                        item
                    }})
            } else {
                // there is is more than one category
                dispatch({type: REMOVE_CATEGORY_FROM_LOCATION, payload: {
                        item
                    }})
            }

        } else {
            // delete from all locations, currentCategory is 'All'
            dispatch({type: DELETE_LOCATION, payload: {
                    item
                }})
        }

        dispatch(setDB('locations'))
        dispatch(filterVisibles());

    }
}
export default deleteLocation;
