import {DELETE_SELECTED_LOCATIONS, REMOVE_CATEGORY_FROM_LOCATION} from 'constants/locations';
import {filterVisibles} from './filterVisibles';
import setDB from '../setDB';
function deleteSelectedLocations() {
    return (dispatch, getState) => {

        //delete selected locations from currentCategory
        const state = getState();
        const {
            categories: {
                data
            },
            locations: {
                currentCategory,
                indexes
            }
        } = state;
        const locationsData = state.locations.data;
        if (data.has(currentCategory)) {
            // delete from single location ('All' is not a real category)
            [...indexes].forEach(index => {
                const item = locationsData.find(x => x.id === index);
                if (item) {
                    dispatch({type: REMOVE_CATEGORY_FROM_LOCATION, payload: {
                            item
                        }})
                }
            })

        } else {
            // delete selected locations from all their categories
            dispatch({type: DELETE_SELECTED_LOCATIONS})
        }

        dispatch(setDB('locations'))
        dispatch(filterVisibles())

    }
}
export default deleteSelectedLocations;
