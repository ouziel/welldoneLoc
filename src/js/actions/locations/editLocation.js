import {EDIT_LOCATION} from 'constants/locations';
import {browserHistory} from 'react-router';

function editLocation(item) {
    browserHistory.push({
        pathname: '/locations',
        query: {
            edit: true,
            name: item.name,
            id: item.id,
            categories: JSON.stringify(item.categories),
            category: item.category,
            address: item.address,
            latitude: item.latitude,
            longitude: item.longitude
        }
    });
    return {type: EDIT_LOCATION, payload: {
            item
        }}
}
export default editLocation;
