import {NEW_LOCATION, LOCATION_EXSITS, ADD_LOCATION, NO_CATEGORIES_CANT_ADD_LOCATION} from 'constants/locations';
import {browserHistory} from 'react-router';
import setDB from '../setDB';
import {filterVisibles} from './filterVisibles';
import Guid from 'guid';
import {notify} from 'react-notify-toast';

function openNewLocation() {
    browserHistory.push({
        pathname: '/locations',
        query: {
            add: true
        }
    });
    return {type: NEW_LOCATION}
}
function openNewLocationFailed() {
    notify.show("Add categories first", 'error', 2500);
    return {type: NO_CATEGORIES_CANT_ADD_LOCATION}
}
function openNew(name) {

    return (dispatch, getState) => {
        const state = getState();
        const categories = state.categories.data;
        if (!categories.size) {
            dispatch(openNewLocationFailed())
        } else {
            dispatch(openNewLocation())

        }
    }
}
function addLocation({
    category,
    categories,
    name,
    address,
    latitude,
    longitude
}) {

    return (dispatch, getState) => {

        const state = getState();
        const locations = state.locations.data;

        if (locations.some(x => (x.name === name && x.categories.some(cat => categories.includes(cat))))) {
            notify.show('Location name already exsits', 'error', 2500);
            dispatch({type: LOCATION_EXSITS})
        } else {
            const id = Guid.raw()
            dispatch({
                type: ADD_LOCATION,
                payload: {
                    id,
                    category,
                    name,
                    address,
                    latitude,
                    longitude,
                    categories
                }
            })
            dispatch(setDB('locations'))
            browserHistory.push({pathname: '/locations'});
            // this is actually just the first category in categories
            // can't move to all new categories at once
            dispatch(filterVisibles(category));
        }

    }
}
export {openNew}
export default addLocation;
