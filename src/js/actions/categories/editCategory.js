import {EDIT_CATEGORY} from 'constants/categories'
import {CHANGE_LOCATION_CATEGORY} from 'constants/locations';
import categoryExsits from './categoryExsits';
export default function editCategory({name, newName}) {

    return (dispatch, getState) => {

        const state = getState();
        const {categories: {
                data
            }} = state;

        if (data.has(newName)) {
            dispatch(categoryExsits());
        } else {
            dispatch({
                type: EDIT_CATEGORY,
                payload: {
                    name,
                    newName
                }
            })

            dispatch({
                type: CHANGE_LOCATION_CATEGORY,
                payload: {
                    name,
                    newName
                }
            })
        }
    }
}
