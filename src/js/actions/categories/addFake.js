import {ADD_FAKE_CATEGORIES} from 'constants/categories';
import addCategory from './addCategory';
function addFakeCategories() {
    return (dispatch, getState) => {
        dispatch({type: ADD_FAKE_CATEGORIES})
        const state = getState();
        const {categories: {
                data
            }} = state;
        const max = 5;
        const chars = 'ABCDEFGHIJKLMNOPQRSTUVXYZ';
        const categories = Array.from(Array(max)).map(item => item = `${chars[(Math.floor(Math.random() * chars.length))]}OO`)
        categories.forEach(category => dispatch(addCategory(category)));

    }
}
export default addFakeCategories
