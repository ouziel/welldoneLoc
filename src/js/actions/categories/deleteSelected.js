import {DELETE_SELECTED_CATEGORIES} from 'constants/categories';
import setDB from '../setDB';
import {filterVisibles} from '../locations/filterVisibles';
import {REMOVE_CATEGORY_LOCATIONS} from 'constants/locations';
export default function deleteSelectedCategories() {
    return (dispatch, getState) => {

        const state = getState();
        const {categories: {
                indexes
            }} = state;
        const entries = [...indexes]

        dispatch({type: DELETE_SELECTED_CATEGORIES})
        entries.forEach(name => {
            dispatch({type: REMOVE_CATEGORY_LOCATIONS, payload: {
                    name
                }})
        })
        dispatch(filterVisibles());
        dispatch(setDB('categories'))

    }
}
