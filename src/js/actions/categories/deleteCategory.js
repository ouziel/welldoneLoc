import {DELETE_CATEGORY} from 'constants/categories';
import {REMOVE_CATEGORY_LOCATIONS} from 'constants/locations';
import setDB from '../setDB';

export default function deleteCategory(name) {

    return (dispatch) => {

        dispatch({type: DELETE_CATEGORY, payload: {
                name
            }})

        dispatch({type: REMOVE_CATEGORY_LOCATIONS, payload: {
                name
            }})

        dispatch(setDB('categories'))
    }
}
