import {ADD_CATEGORY} from 'constants/categories';
import categoryExsits from './categoryExsits';
import setDB from '../setDB';

function addCategory(name) {

    return (dispatch, getState) => {

        const state = getState();
        const categories = state.categories.data;
        if (categories.has(name)) {
            dispatch(categoryExsits())
        } else {
            dispatch({type: ADD_CATEGORY, payload: {
                    name
                }})
            dispatch(setDB('categories'))
        }
    }
}

export default addCategory
