import {CATEGORY_EXSITS} from 'constants/categories';
import {notify} from 'react-notify-toast';

function categoryExsits(name) {
    notify.show('Category exsits', 'error', 2500);
    return {type: CATEGORY_EXSITS, payload: {
            name
        }}

}

export default categoryExsits;
