import 'babel-polyfill'
//////////////////////////////
///          REACT CORE
/////////////////////////////
import React, {PropTypes} from 'react'
import ReactDOM from 'react-dom'
import {browserHistory, Router, Route, IndexRoute, Link} from 'react-router'
//////////////////////////////
///          APP CORE
/////////////////////////////
import config from 'config';
import './sass/style.css';
//////////////////////////////
///          PAGES
/////////////////////////////
import {Index} from 'pages';
import {CategoriesContainer, LocationsContainer, AppContainer} from 'containers';
//////////////////////////////
///          STORE
/////////////////////////////
import {Provider} from 'react-redux'
import store, {history} from './store';
//////////////////////////////
///          APP TREE
/////////////////////////////
ReactDOM.render(
    <Provider store={store}>
    <Router history={history}>
        <Route path="/" component={AppContainer}>
            <IndexRoute component={Index}/>
            <Route path="/categories" component={CategoriesContainer}/>
            <Route path="/locations" component={LocationsContainer}/>
        </Route>
    </Router>
</Provider>, document.getElementById('root'));
